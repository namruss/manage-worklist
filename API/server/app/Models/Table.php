<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'status'
    ];
    /**
     * Get all of the listCard for the Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listCard()
    {
        return $this->hasMany(ListCard::class, 'table_id', 'id');
    }
}

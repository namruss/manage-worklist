<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\Table\TableRepositoryInterface::class,
            \App\Repositories\Table\TableRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\ListCard\ListCardRepositoryInterface::class,
            \App\Repositories\ListCard\ListCardRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserRepository::class
        );
        $this->app->singleton(
            \App\Repositories\User\CommentRepositoryInterface::class,
            \App\Repositories\User\CommentRepository::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthenticationException) {
            return response()->json(['message' => 'Unauthenticated'], 401);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json(['message' => 'Route not found'], 404);
        }

        if ($exception instanceof ModelNotFoundException) {
            return response()->json(['message' => 'Model not found'], 404);
        }

        if ($exception instanceof ValidationException) {
            $messages = collect(array_values($exception->errors()))->flatten();

            return response()->json([
                'message' => count($messages) ? $messages[0] : $exception->errors(),
                'errors' => $exception->errors(),
            ], 422);
        }

        return response()->json(
            ['message' => $exception->getMessage()],
            method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500
        );
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (NotFoundHttpException $e, $request) {
            if ($request->is('api/posts/*')) {
                return response()->json([
                    'message' => 'Record not found.'
                ], 404);
            }
        });
    }
}

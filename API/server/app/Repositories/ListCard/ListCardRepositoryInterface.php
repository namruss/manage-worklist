<?php
namespace App\Repositories\ListCard;

use App\Repositories\RepositoriesInterface;

interface ListCardRepositoryInterface extends RepositoriesInterface
{
    /**      * Find data by multiple fields      *     
    * * @param array $where      * @param array $columns      *    
    * @return mixed */    
    public function search($params);
}
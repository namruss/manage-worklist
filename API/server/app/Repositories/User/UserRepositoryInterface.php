<?php

namespace App\Repositories\User;

use App\Repositories\RepositoriesInterface;

interface UserRepositoryInterface extends RepositoriesInterface
{
    /**
     * Find data by multiple fields
     * @param array $where 
     * @param array $columns
     * @return mixed      
     * */    
    public function search($params);
}
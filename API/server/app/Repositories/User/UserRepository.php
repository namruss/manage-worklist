<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepositories;

class UserRepository extends BaseRepositories implements UserRepositoryInterface
{
    /**
     * Assign User Model
     */
    public function model()
    {
        return \App\Models\User::class;
    }

    /**
     * Find data by multiple fields
     * @param array $where 
     * @param array $columns
     * @return mixed      
     * */     
    public function search($params)
    {
        $conditions = $this->getSearchConditions($params);
        $conditionsFormated = [];
        if (isset($conditions['name'])) {
            $conditionsFormated[] = ['full_name', 'like', '%' . $params['name'] . '%'];
        }
        if (isset($conditions['email'])) {
            $conditionsFormated[] = ['email', 'like', '%' . $params['email'] . '%'];
        }
        // if (isset($conditions['type'])) {
        //     $conditionsFormated[] = ['type', '=', (int) $conditions['type']];
        // }
        $params['conditions'] = $conditionsFormated;
        $result = $this->searchByParams($params);
        return $result;
    }
}

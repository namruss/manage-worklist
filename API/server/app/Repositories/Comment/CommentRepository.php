<?php

namespace App\Repositories\Comment;

use App\Repositories\BaseRepositories;

class CommentRepository extends BaseRepositories implements CommentRepositoryInterface
{
    /**
     * Assign Comment Model
     */
    public function model()
    {
        return \App\Models\Comment::class;
    }
}
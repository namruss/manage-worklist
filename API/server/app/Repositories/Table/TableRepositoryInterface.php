<?php
namespace App\Repositories\Table;

use App\Repositories\RepositoriesInterface;

interface TableRepositoryInterface extends RepositoriesInterface
{
    /**      * Find data by multiple fields      *     
    * * @param array $where *
    * @param array $columns *    
    * @return mixed */    
    public function search($params);
}
<?php
namespace App\Repositories\Table;

use App\Repositories\BaseRepositories;

class TableRepository extends BaseRepositories implements TableRepositoryInterface
{
    //lấy model tương ứng
    public function Model()
    {
        return \App\Models\Table::class;
    }

    
    /**      * Find data by multiple fields      *     
    * * @param array $where      * @param array $columns      *    
    * @return mixed */    
    public function search($params)
    {
        $conditions = $this->getSearchConditions($params);
        $conditionsFormated = [];
        if (isset($conditions['title'])) {
            $conditionsFormated[] = ['title', 'like', '%' . $params['title'] . '%'];
        }
        $params['conditions'] = $conditionsFormated;
        $result = $this->searchByParams($params);
        return $result;
    }

}
<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use App\Repositories\ListCard\ListCardRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Requests\ListCardRequest;
use App\Http\Resources\ListCardResource;
use Error;

class ListCardController extends Controller
{
    protected $listCardRepo;

    public function __construct(ListCardRepositoryInterface $listCardRepo)
    {
        $this->listCardRepo = $listCardRepo;
    }

    /**
     * create new entity 
     * @param $request
     * @return boolean
     */
    public function create(ListCardRequest $request){
        try {
            $data = $request->all();
            return $this->listCardRepo->create($data);
        } catch (Error $err) {
            throw ($err);
        }
        
    }

    /**
     * save data has been updated
     * @param $request, $id
     * @return mixed
     */
    public function store(ListCardRequest $request, $id){
        try {
            $data = $request->all();
            return new ListCardResource($this->listCardRepo->update($data,$id));
        } catch (Error $err) {
            throw ($err);
        } 
    }

    /**
     * delete entity by id
     * @param $id
     * @return boolean
     */
    public function delete($id){
        try {
            return $this->listCardRepo->delete($id);
        } catch (Error $err) {
            throw ($err);
        }
    }

    /**
     * get data by muliple field
     * @param $request
     * @return mixed
     */
    public function index(Request $request){
        try {
            $params = $request->all();
            $params['conditions'] = $request->all();
            $data = $this->listCardRepo->search($params);
            return ListCardResource::collection($data);
        } catch (Error $err) {
            throw ($err);
        }
    }

    /**
     * search entity by id
     * @param $id
     * @return mixed
     */
    public function searchById($id){
        try {
            $data = $this->listCardRepo->findOrFail($id);
            return new ListCardResource($data);
        } catch (Error $err) {
            throw ($err);
        }
    }
}

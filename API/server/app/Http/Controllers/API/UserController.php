<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    protected $userRepository;

    /**
     * Init Instant User Repository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get List User Or Search by Params
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['conditions'] = $request->all();
        
        return UserResource::collection($this->userRepository->search($params));
    }

    /**
     * Store a new User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function store(UserRequest $request)
    {
        try {
            $user = $this->userRepository->create($request->all());
        }catch (\Throwable $except) {
            return response()->json(['error' => $except], 500);
        }
        return new UserResource($user);
    }

    /**
     * Get one User by ID.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $user = $this->userRepository->findOrFail($id);
        return new UserResource($user);
    }

    /**
     * Update User by ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return mixed
     */
    public function update(UserRequest $request, $id)
    {
        try {
            $user = $this->userRepository->update($request->all(), $id);
        }catch (\Throwable $except) {
            return response()->json(['error' => $except], 500);
        }
        return new UserResource($user);
    }

    /**
     * Remove User By ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = $this->userRepository->findOrFail($id);
            $user->delete();
        }catch (\Throwable $except) {
            return response()->json(['error' => $except], 500);
        }
        
        return response()->json(['status' => true,'user' => new UserResource($user)]);
    }
}

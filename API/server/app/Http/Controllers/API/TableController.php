<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TableRequest;
use App\Models\Table;
use App\Repositories\Table\TableRepositoryInterface;
use App\Http\Resources\TableResource;
use Error;

class TableController extends Controller
{
    protected $tableRepo;

    public function __construct(TableRepositoryInterface $tableRepo)
    {
        $this->tableRepo = $tableRepo;
    }

    /**
     * create new entity 
     * @param $request
     * @return boolean
     */
    public function create(TableRequest $request){
        try {
            $data = $request->all();
            return $this->tableRepo->create($data);
        } catch (Error $err) {
            throw ($err);
        }
    }

    /**
     * save entity
     * @param $request, $id
     * @return mixed
     */
    public function store(TableRequest $request, $id){
        try {
            $data = $request->all();
            return new TableResource($this->tableRepo->update($data,$id));
        } catch (Error $err) {
            throw $err;
        }
    }

    /**
     * delete entity
     * @param $id
     * @return boolean
     */
    public function delete($id){
        try {
            return $this->tableRepo->delete($id);
        } catch (Error $err) {
            throw $err;
        }
    }

    /**
     * get data by multi field
     * @param $request
     * @return mixed
     */
    public function index(Request $request){
        try {
            $params = $request->all();
            $params['conditions'] = $request->all();
            $data = $this->tableRepo->search($params);
            return TableResource::collection($data);
        } catch (Error $err) {
            throw ($err);
        }
    }

    /**
     * search entity by id
     * @param $id
     * @return mixed
     */
    public function searchById($id){
        try {
            return new TableResource($this->tableRepo->findOrFail($id));
        } catch (Error $err) {
            throw $err;
        }
    }

}

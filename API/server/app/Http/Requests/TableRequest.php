<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class TableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'title' => 'required|unique:tables,title',
                ];
                break;

            case 'PUT':
                return [
                    'title' => 'required|unique:tables,title,'.$this->id,
                ];
                break;
            
            default:
                # code...
                break;
            }
    }

    public function messages(){
        return [
            'title.required' => 'Title không được để trống',
            'title.unique' => 'Title đã tồn tại, xin đặt tên khác',
            'position.numeric' => 'Vị trí phải là số'
        ];
    }

    
    /**      
     * * Handle a failed validation attempt.      *      
     * * @param  \Illuminate\Contracts\Validation\Validator  $validator      
     * * @return void      *      * @throws \Illuminate\Validation\ValidationException      
     * */     
    protected function failedValidation(Validator $validator)    
     {      
            $errors = ['validate' => (new ValidationException($validator))->errors()];        
            throw new HttpResponseException(         
                response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)      
            );     
    }
}

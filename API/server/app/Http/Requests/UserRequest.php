<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
                'full_name' => 'required|min:8|max:50',
                'email' => 'required|email',
                'phone_number' => 'required|min:8|max:12|regex:/^[0-9]+$/',
                'password' => 'required|min:8|max:50',
                'role' => 'required|numeric'
            ];
    }

    /**
     * Set errors the validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'full_name.required' => 'Họ và Tên không được để trống',
            'full_name.min' => 'Họ và Tên quá ngắn',
            'full_name.max' => 'Họ và Tên quá dài',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại',
            'phone_number.required' => 'Số điện thoại không được để trống',
            'phone_number.min' => 'Số điện thoại không đúng định dạng',
            'phone_number.max' => 'Số điện thoại không đúng định dạng',
            'phone_number.regex' => 'Số điện thoại không đúng định dạng',
            'password.required' => 'Password không được để trống',
            'password.min' => 'Password phải có ít nhất 8 ký tự',
            'password.max' => 'Password quá dài',
            'role.required' => 'Role không được bỏ trống',
            'role.numeric' => 'Role không đúng định dạng'
        ];
    }

    /**
     *Handle a failed validation attempt.
     *@param  \Illuminate\Contracts\Validation\Validator  $validator      
     *@return void      
     *     
     *@throws \Illuminate\Validation\ValidationException
     */     
    protected function failedValidation(Validator $validator)
    {         
        $errors = ['validate' => (new ValidationException($validator))->errors()];
        throw new HttpResponseException(response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}

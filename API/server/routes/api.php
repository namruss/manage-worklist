<?php
use Illuminate\Http\Request;

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\CommentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\TableController;
use App\Http\Controllers\API\ListCardController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('App\Http\Controllers\API')->group(function(){
    //Table
    Route::post('/table',[TableController::class, 'create']);
    Route::get('table/{id}',[TableController::class,'searchById']);
    Route::put('/table/{id}',[TableController::class, 'store']);
    Route::delete('/table/{id}',[TableController::class, 'delete']);
    Route::get('table',[TableController::class,'index']);

    //ListCard
    Route::post('/list-card',[ListCardController::class, 'create']);
    Route::get('list-card/{id}',[ListCardController::class,'searchById']);
    Route::put('/list-card/{id}',[ListCardController::class, 'store']);
    Route::delete('/list-card/{id}',[ListCardController::class, 'delete']);
    Route::get('list-card',[ListCardController::class,'index']);

});
// Route::namespace('App\Http\Controllers')->group(function(){
//     Route::resource('user', 'API\UserController');
// });

// User
Route::get('user', [UserController::class, "index"]);
Route::get('user/{id}', [UserController::class, "show"]);
Route::post('user', [UserController::class, "store"]);
Route::put('user/{id}', [UserController::class, "update"]);
Route::delete('user/{id}', [UserController::class, "destroy"]);

// Comment
Route::get('comment', [CommentController::class, "index"]);
Route::get('comment/{id}', [CommentController::class, "show"]);
Route::post('comment', [CommentController::class, "store"]);
Route::put('comment/{id}', [CommentController::class, "update"]);
Route::delete('comment/{id}', [CommentController::class, "destroy"]);

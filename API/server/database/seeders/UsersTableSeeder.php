<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['full_name'=>'Nguyen Van Thang', 'email'=>'ngvthang@gmail.com', 'phone_number'=> '0339035050', 'role'=>1, 'password'=>'876543'],
            ['full_name'=>'Nguyen A', 'email'=>'ngva@gmail.com', 'phone_number'=> '0449033636', 'role'=>0, 'password'=>'876543'],
        ]);
    }
}
